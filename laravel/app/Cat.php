<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cat extends Model
{
    public static function getAllCats()
    {
      return self::all();
    }
    
    public static function getOneCat($id)
    {
      return self::where('id', $id)->firstOrfail();
    }
}
