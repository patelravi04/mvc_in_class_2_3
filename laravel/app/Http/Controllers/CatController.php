<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Cat;

class CatController extends Controller
{
    public function index()
    {
      $cats = Cat::getAllCats();
      return view('katie.cats', compact('cats'));      
    }
    
    public function show($id)
    {
      $cats = Cat::getOneCat($id);
      return view('katie.catdetails', compact('cats'));      
    }
}
