<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class People extends Model
{
    public static function getAllPeople(){
        return self::all();
    }   

    public static function getOne($id){
        return self::where('id', $id)->firstOrfail();
    }   
}
