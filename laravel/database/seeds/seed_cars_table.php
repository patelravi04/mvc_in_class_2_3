<?php

use Illuminate\Database\Seeder;

class seed_cars_table extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cars')->insert([
        	'name' => 'Tesla',
        	'description' => 'Tesla is a luxurious electronic car',
        	'image' => 'tesla.jpg'
        ]);
        DB::table('cars')->insert([
        	'name' => 'BMW',
        	'description' => 'This car is from BMW',
        	'image' => 'bmw.jpg'
        ]);
        DB::table('cars')->insert([
        	'name' => 'Lamborghini',
        	'description' => 'This car is from Lamborghini',
        	'image' => 'lamborghini.jpg'
        ]);
        DB::table('cars')->insert([
        	'name' => 'Bugati',
        	'description' => 'This car is from Bugati',
        	'image' => 'bugati.jpg'
        ]);
        DB::table('cars')->insert([
        	'name' => 'Mercedes',
        	'description' => 'This car is from Mercedes',
        	'image' => 'mercedes.jpg'
        ]);
        //dsdfg
    }
}
