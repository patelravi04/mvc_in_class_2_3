<?php

use Illuminate\Database\Seeder;

class seed_people extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('people')->insert([
            'name' => 'Luka Doncic',
            'description' => 'Basketball Player, Dallas Mavericks',
            'image' => 'Luka.jpg'
        ]);
        DB::table('people')->insert([
            'name' => 'Kristaps Porzingis',
            'description' => 'Basketball Player, Dallas Mavericks',
            'image' => 'Kristaps.jpg'
        ]);
        DB::table('people')->insert([
            'name' => 'Jimmy Butler',
            'description' => 'Basketball Player, Miami Heat',
            'image' => 'Jimmy.jpg'
        ]);
        DB::table('people')->insert([
            'name' => 'Lebron James',
            'description' => 'Basketball Player, Los Angeles Lakers',
            'image' => 'Lebron.jpg'
        ]);
        DB::table('people')->insert([
            'name' => 'Tyler Herro',
            'description' => 'Basketball Player, Miami Heat',
            'image' => 'Tyler.jpg'
        ]);
    }
}
